# jgifresize

some sample code for re-encoding gifs in java

you'll be interested in this if you're trying to work with
animated gifs in java given that the standard ImageIO gif encoder
is bugged and you don't want to piece together bits of pieces
of solutions lying around the internet
