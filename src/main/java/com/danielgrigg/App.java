package com.danielgrigg;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.*;

public class App {


    public static void main(String[] args) {
        try {

            String inputName = args[0];
            FileInputStream fis = new FileInputStream(inputName);
            GifDecoder.GifImage gif = GifDecoder.read(fis);
            int width = gif.getWidth();
            final int height = gif.getHeight();
            final int background = gif.getBackgroundColor();
            final int frameCount = gif.getFrameCount();

            System.out.printf("Input: width=%d, height=%d, background=%d, frameCount=%d, imageType=%d\n", width, height, background, frameCount, gif.getFrame(0).getType());
            final String baseName = inputName.substring(0, inputName.lastIndexOf('.'));
            new File(baseName).mkdir();

            for (int i = 0; i < frameCount; i++) {
                final BufferedImage img = gif.getFrame(i);
                final int delay = gif.getDelay(i);
                final String outPath = baseName + "/" + baseName + "_" + i + ".png";
                System.out.printf("output: %s, delay=%d, disposalMethod=%s\n", outPath, delay, gif.getDisposalMethod(i));
                ImageIO.write(img, "png", new File(outPath));
            }


            new File("out").mkdir();
            ImageOutputStream outputStream = new FileImageOutputStream(new File("out" + "/" + inputName));
            GifSequenceWriter writer = new GifSequenceWriter(outputStream,
                    gif.getFrame(0).getType(),
//                        gif.getDelay(0) * 10,
                    true
//                        gif.getDisposalMethod(0)
            );

            for (int i = 0; i < gif.getFrameCount(); i++) {
                writer.writeToSequence(gif.getFrame(i), gif.getDelay(i) * 10, gif.getDisposalMethod(i));
            }
            writer.close();
            outputStream.close();

//            {
//                ByteArrayOutputStream bos = new ByteArrayOutputStream(256);
//                ImageOutputStream outputStream = ImageIO.createImageOutputStream(bos);
//
//                GifSequenceWriter writer = new GifSequenceWriter(outputStream, gif.getFrame(0).getType(), gif.getDelay(0) * 10, true);
//
//                for (int i = 0; i < gif.getFrameCount(); i++) {
//                    writer.writeToSequence(gif.getFrame(i));
//                }
//                writer.close();
//                outputStream.close();
//                bos.close();
//                InputStream is = new ByteArrayInputStream(bos.toByteArray());
//                System.out.println("Read to array");
//                is.close();
//            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("IO Exception: ");
            e.printStackTrace();
        }
    }
}
