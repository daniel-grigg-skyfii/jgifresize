package com.danielgrigg;

// See https://docs.oracle.com/en/java/javase/14/docs/api/java.desktop/javax/imageio/metadata/doc-files/gif_metadata.html
public enum GifDisposalMethod {
    none,
    doNotDispose,
    restoreToBackgroundColor,
    restoreToPrevious,
}
